from django.contrib import admin
from .models import Producto
# Register your models here.

class ProductoAdmin(admin.ModelAdmin):
    list_display = ["nombre", "precioA", "precioD", "nuevo"]
    list_editable = ["precioA", "precioD", "nuevo"]
    search_fields = ["nombre"]
    

admin.site.register(Producto, ProductoAdmin)