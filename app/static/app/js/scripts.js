$(document).ready(function(){

  $('#search-field').on('keypress', function (e) {
    if(e.which === 13){
      window.location.href = 'explorar.html';
    }
  });
});

$("#btnRegistrar").click(function() {
  if($("#nombre").val() == ""){
    alert("Ingrese nombre")
    return false;
  }
  if ($("#nombre").val().length < 5) {
    alert("El nombre debe tener como mínimo 5 caracteres");
    return false;
  }
  if($("#correo").val() == ""){
    alert("Ingrese correo")
    return false;
  }
  if($("#pass1").val() == ""){
    alert("Ingrese contraseña")
    return false;
  }
  if ($("#pass1").val().length < 6) {
    alert("La contraseña debe tener como mínimo 6 caracteres");
    return false;
  }
  if($("#pass2").val() == ""){
    alert("Ingrese confirmación de contraseña")
    return false;
  }
  if ($("#pass2").val().length < 6) {
    alert("La confirmación de la contraseña debe tener como mínimo 6 caracteres");
    return false;
  }
  if ($("#pass1").val() != $("#pass2").val()) {
    alert("Las contraseñas no coinciden");
    return false;
  }
 
  if($("#edad").val() == ""){
    alert("Ingrese edad")
    return false;
  }
  if($("#usuario").val() == ""){
    alert("Ingrese nombre")
    return false;
  }
  if ($("#usuario").val().length < 5) {
    alert("El usuario debe tener como mínimo 5 caracteres");
    return false;
  }
  return true;
});



