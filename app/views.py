from django.shortcuts import render, redirect, get_object_or_404
from .models import Producto
from .forms import ProductoForm, CustomUserCreationForm
from django.contrib.auth import authenticate, login
from django.contrib import messages
from rest_framework import viewsets
from .serializers import ProductoSerializer

# Create your views here.
def error_facebook(request):
    return render(request, 'registration/error_facebook.html')
class ProductoViewset(viewsets.ModelViewSet):
    queryset = Producto.objects.all()
    serializer_class = ProductoSerializer

    def get_queryset(self):
        productos = Producto.objects.all()

        nombre = self.request.GET.get("nombre")

        if nombre:
            productos = productos.filter(nombre__contains=nombre)
        return productos    

def index(request):
    return render (request, 'app/index.html')

def iniciarS(request):
    return render (request, 'app/iniciarS.html')

def registrar (request):
    return render (request, 'app/registrar.html')

def explorar (request):
    productos = Producto.objects.all()
    data = {
        'productos': productos
    }
    return render (request, 'app/explorar.html', data)

def agregar_producto (request):

    data = {
        'form': ProductoForm()
    }
    if request.method == 'POST':
        formulario = ProductoForm(data=request.POST, files=request.FILES)
        if formulario.is_valid():
            formulario.save()
            data["mensaje"] = "guardado correctamente"
        else:
            data["form"] = formulario

    return render (request, 'app/producto/agregar.html', data)    

def listar_producto(request):
    productos = Producto.objects.all()

    data = {
        'productos': productos
    }
    return render(request, 'app/producto/listar.html', data)

def modificar_producto(request, id):

    producto = get_object_or_404(Producto, id=id)
    data ={
        'form': ProductoForm(instance=producto)
    }
    if request.method == 'POST':
        formulario = ProductoForm(data=request.POST, instance=producto, files=request.FILES)
        if formulario.is_valid():
            formulario.save()
            messages.success(request,"modificado correctamente")
            return redirect(to="listar_producto")
        data["form"] = formulario    
    return render(request, 'app/producto/modificar.html',data)    

def eliminar_producto(request, id):
    producto = get_object_or_404(Producto, id=id)
    producto.delete()
    messages.success(request,"eliminado correctamente")
    return redirect(to="listar_producto")


def registro(request):
    data = {
        'form': CustomUserCreationForm()
    }
    if request.method == 'POST':
        formulario = CustomUserCreationForm(data=request.POST)
        if formulario.is_valid():
            formulario.save()
            user = authenticate(username= formulario.cleaned_data["username"], password=formulario.cleaned_data["password1"])
            login(request, user)
            messages.success(request, "Te has registrado correctamente")
            return redirect(to="index")
        data["form"] = formulario   
    return render(request,'registration/registro.html', data)   