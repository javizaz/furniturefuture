from django.urls import path, include
from .views import index, explorar, agregar_producto, listar_producto, modificar_producto, eliminar_producto, registro, ProductoViewset, error_facebook
from rest_framework import routers

router = routers.DefaultRouter()
router.register('producto', ProductoViewset)

urlpatterns = [
    path('',index, name= "index"),
    path ('explorar.html/',explorar, name= "explorar"),
    path ('agregar-producto/',agregar_producto, name= "agregar_producto"),
    path ('listar-producto/', listar_producto, name= "listar_producto"),
    path ('modificar-producto/<id>/', modificar_producto, name= "modificar_producto"),
    path ('eliminar-producto/<id>/', eliminar_producto, name= "eliminar_producto"),
    path ('registro/', registro, name="registro"),
    path ('api/', include(router.urls)),
    path ('error-facebook/', error_facebook, name="error_facebook"),
]
